import React, { useState } from "react";
import ReactMapboxGl, { Layer, Feature, Marker, Popup } from "react-mapbox-gl";
import styled from "styled-components";
import data from "./data";

const Map = ReactMapboxGl({
  accessToken:
    "pk.eyJ1IjoiZHdpdmVkaXRhbnVqNDEiLCJhIjoiY2syd3hudHJmMDZqNzNqcGZsaDgxcGxhaiJ9._svnz_54fancTvQycDqRDg"
});

const points = data;
const App = () => {
  const [popup, setpopup] = useState(null);
  console.log(popup);
  return (
    <div>
      <Map
        style="mapbox://styles/mapbox/streets-v9"
        containerStyle={{
          height: "100vh",
          width: "100vw"
        }}
        center={[-1.53499813, 53.80679386]}
      >
        {points.map(point => {
          // console.log(point);
          return (
            <Marker coordinates={[point.long, point.lat]} anchor="bottom">
              <MarkerIcon
                src={require("./comment-marker.svg")}
                onClick={() => {
                  setpopup(point);
                }}
              />{" "}
            </Marker>
          );
        })}

        {popup && (
          <Popup
            tipSize={5}
            anchor="top"
            coordinates={[popup.long, popup.lat]}
            closeOnClick={true}
            onClose={() => this.setState({ popupInfo: null })}
          >
            Road Name: {popup.road_name} <br />
            Length in kms: {popup.length} <br />
            AADT: {popup.AADT} <br />
            Actual accidents: {popup.yr_acc} <br />
            Estimated accidents: {popup.est} <br />
            Estimate std: {popup.est_dev} <br />
          </Popup>
        )}
      </Map>
      ;
    </div>
  );
};

export default App;

const MarkerIcon = styled.img`
  width: 2.5em;
  display: block;
`;
