const data = [
  {
    road_name: "A1(M)",
    lat: 53.84948972,
    long: -1.34322541,
    length: 0.5,
    AADT: 103.01095890410959,
    yr_acc: 176,
    est: 176.42,
    est_dev: 13.282319074619462
  },
  {
    road_name: "A168",
    lat: 53.92186829,
    long: -1.38535016,
    length: 1.9,
    AADT: 53.49452054794521,
    yr_acc: 4,
    est: 5.764,
    est_dev: 2.40083318870762
  },
  {
    road_name: "A58",
    lat: 53.75232136,
    long: -1.67557636,
    length: 0.1,
    AADT: 35.87260273972603,
    yr_acc: 335,
    est: 335.084,
    est_dev: 18.305299779025745
  },
  {
    road_name: "A6038",
    lat: 53.86687725,
    long: -1.72396132,
    length: 0.3,
    AADT: 59.582191780821915,
    yr_acc: 8,
    est: 8.252,
    est_dev: 2.872629457483161
  },
  {
    road_name: "A61",
    lat: 53.80679386,
    long: -1.53499813,
    length: 0.2,
    AADT: 27.0013698630137,
    yr_acc: 334,
    est: 334.16799999994305,
    est_dev: 18.280262580167882
  },
  {
    road_name: "A6110",
    lat: 53.77677195,
    long: -1.58343225,
    length: 0.3,
    AADT: 118.53287671232877,
    yr_acc: 129,
    est: 129.336,
    est_dev: 11.372598647626672
  },
  {
    road_name: "A6120",
    lat: 53.79276163,
    long: -1.42843732,
    length: 0.4,
    AADT: 104.26301369863013,
    yr_acc: 274,
    est: 274.336,
    est_dev: 16.56309149887182
  },
  {
    road_name: "A62",
    lat: 53.74796902,
    long: -1.63718475,
    length: 1.0,
    AADT: 94.56986301369864,
    yr_acc: 108,
    est: 108.84,
    est_dev: 10.43264108459598
  },
  {
    road_name: "A63",
    lat: 53.79733831,
    long: -1.42130041,
    length: 0.3,
    AADT: 10.772602739726027,
    yr_acc: 124,
    est: 115.1816564816872,
    est_dev: 10.33312364239731
  },
  {
    road_name: "A639",
    lat: 53.747131499999995,
    long: -1.45407654,
    length: 0.5,
    AADT: 50.61917808219178,
    yr_acc: 67,
    est: 67.42,
    est_dev: 8.210968274204937
  },
  {
    road_name: "A64",
    lat: 53.79840988,
    long: -1.52856254,
    length: 0.3,
    AADT: 95.23150684931507,
    yr_acc: 114,
    est: 114.252,
    est_dev: 10.688872718860488
  },
  {
    road_name: "A642",
    lat: 53.74982779,
    long: -1.4540416,
    length: 0.3,
    AADT: 49.55205479452055,
    yr_acc: 47,
    est: 47.252,
    est_dev: 6.874009019487827
  },
  {
    road_name: "A643",
    lat: 53.74204531,
    long: -1.62819936,
    length: 0.9,
    AADT: 22.976712328767125,
    yr_acc: 69,
    est: 69.75599999771754,
    est_dev: 8.352005746851177
  },
  {
    road_name: "A647",
    lat: 53.79596020000001,
    long: -1.57033826,
    length: 0.7,
    AADT: 75.3890410958904,
    yr_acc: 140,
    est: 140.588,
    est_dev: 11.856981066021822
  },
  {
    road_name: "A65",
    lat: 53.88018778,
    long: -1.72767663,
    length: 0.9,
    AADT: 57.42191780821918,
    yr_acc: 233,
    est: 233.756,
    est_dev: 15.28908107114355
  },
  {
    road_name: "A650",
    lat: 53.75514846,
    long: -1.67737457,
    length: 0.7,
    AADT: 39.893150684931506,
    yr_acc: 107,
    est: 107.588,
    est_dev: 10.372463545368573
  },
  {
    road_name: "A653",
    lat: 53.78525955,
    long: -1.54464608,
    length: 0.5,
    AADT: 58.9041095890411,
    yr_acc: 195,
    est: 195.42,
    est_dev: 13.979270367225894
  },
  {
    road_name: "A654",
    lat: 53.75166639,
    long: -1.46311694,
    length: 1.3,
    AADT: 28.57123287671233,
    yr_acc: 39,
    est: 40.09199999999993,
    est_dev: 6.331824381645455
  },
  {
    road_name: "A656",
    lat: 53.78256825,
    long: -1.34889932,
    length: 3.3,
    AADT: 25.65753424657534,
    yr_acc: 30,
    est: 32.771999999996765,
    est_dev: 5.724683397358628
  },
  {
    road_name: "A657",
    lat: 53.82960386,
    long: -1.69761798,
    length: 2.8,
    AADT: 49.49589041095891,
    yr_acc: 26,
    est: 28.352,
    est_dev: 5.324659613533996
  },
  {
    road_name: "A658",
    lat: 53.90280862,
    long: -1.62962053,
    length: 0.4,
    AADT: 51.08904109589041,
    yr_acc: 71,
    est: 71.336,
    est_dev: 8.446064172145508
  },
  {
    road_name: "A659",
    lat: 53.90246382,
    long: -1.70287603,
    length: 0.5,
    AADT: 39.46986301369863,
    yr_acc: 35,
    est: 35.42,
    est_dev: 5.951470406546604
  },
  {
    road_name: "A660",
    lat: 53.80334234,
    long: -1.54671269,
    length: 0.3,
    AADT: 66.38219178082191,
    yr_acc: 218,
    est: 222.284,
    est_dev: 14.90919179566753
  },
  {
    road_name: "A660",
    lat: 53.81046885,
    long: -1.55606662,
    length: 5.1,
    AADT: 66.38219178082191,
    yr_acc: 218,
    est: 222.284,
    est_dev: 14.90919179566753
  },
  {
    road_name: "A661",
    lat: 53.92996579,
    long: -1.39229791,
    length: 2.3,
    AADT: 29.464383561643835,
    yr_acc: 4,
    est: 5.931999999999999,
    est_dev: 2.435569748539343
  },
  {
    road_name: "M1",
    lat: 53.7989483,
    long: -1.41781668,
    length: 0.3,
    AADT: 9.293150684931506,
    yr_acc: 264,
    est: 135.82594205585414,
    est_dev: 8.355521949110472
  },
  {
    road_name: "M62",
    lat: 53.72849935,
    long: -1.44825504,
    length: 2.5,
    AADT: 311.7753424657534,
    yr_acc: 299,
    est: 301.1,
    est_dev: 17.352233285660954
  },
  {
    road_name: "M621",
    lat: 53.78318315,
    long: -1.5422403,
    length: 0.4,
    AADT: 214.13150684931506,
    yr_acc: 186,
    est: 186.336,
    est_dev: 13.65049449653748
  }
];
export default data;
