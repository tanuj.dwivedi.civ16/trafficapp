import pandas as pd
import math
import json

#=====IMPORTING DATA=====#
df_17 = pd.read_csv("./Data/Leeds_RTC_2017.csv",usecols=['Number of Vehicles','1st Road Class & No'])
df_18 = pd.read_csv("./Data/RTC_2018_Leeds.csv",usecols=['Number of Vehicles','1st Road Class & No'])

df_road = pd.read_csv("./Data/dft_aadf_local_authority_id_63_1.csv",usecols=['year','road_name','latitude','longitude','link_length_km','all_motor_vehicles'])



#=====CALCULATE TOTAL CASUALTIES WITHIN ROAD SEGMENT=====#
r_class = df_road['road_name']

tally_17={} #tally_17 calculated the total number of casualties per road segment during the year 2017s
for key in r_class:
	tally_17[key]=0


tally_18={}
tally_18=dict.fromkeys(tally_17.keys(),0)

for row_index,row in df_17.iterrows():
	if row['1st Road Class & No'] in tally_17:
		tally_17[row['1st Road Class & No']]=tally_17[row['1st Road Class & No']]+row['Number of Vehicles']




for row_index,row in df_18.iterrows():
	if row['1st Road Class & No'] in tally_18:
		tally_18[row['1st Road Class & No']]=tally_18[row['1st Road Class & No']]+row['Number of Vehicles']

zero_el=[];
for key in tally_17:
	if key not in tally_18:
		zero_el.append(key)
	elif tally_17[key]==0 and tally_18[key]==0:
		zero_el.append(key)

for i in zero_el:
	tally_17.pop(i,None)
	tally_18.pop(i,None)

#=====EMPIRICAL BAYESIAN=====#
a=-15.22
b=1.68
k=0.84

AADT={}
AADT=dict.fromkeys(tally_17.keys(),0)
mu={}
mu=dict.fromkeys(tally_17.keys(),0)
length={}
length=dict.fromkeys(tally_17.keys(),0)
W={}
W=dict.fromkeys(tally_17.keys(),0)
est={}
est=dict.fromkeys(tally_17.keys(),0)
est_dev={}
est_dev=dict.fromkeys(tally_17.keys(),0)


for row_index,row in df_road.iterrows():
	if row['road_name'] in tally_17:
		AADT[row['road_name']]=AADT[row['road_name']]+row['all_motor_vehicles']
		length[row['road_name']]=row['link_length_km']



for key in AADT:
 	AADT[key]=AADT[key]/(2*365)

for key in mu:
	mu[key]=math.exp(a+b*AADT[key]+math.log(length[key]))

for key in W:
	W[key]=1/(1+(mu[key]*2)/k)

for key in est:
	est[key]=W[key]*mu[key]*2*length[key]+(1-W[key])*(tally_17[key]+tally_18[key])

for key in est_dev:
	est_dev[key]=math.sqrt((1-W[key])*est[key])

#=====CREATE JSON OBJECT TO SERVE CLIENT=====#
json_dump=[]


for row_index,row in df_road.iterrows():
	row_dict={}
	if row['year']==2017:
		if row['road_name'] in tally_17:
			row_dict['road_name']=row['road_name']
			row_dict['lat']=row['latitude']
			row_dict['long']=row['longitude']
			row_dict['length']=row['link_length_km']
			row_dict['AADT']=AADT[row['road_name']]
			row_dict['2_yr_acc']=tally_17[row['road_name']]+tally_18[row['road_name']]
			row_dict['est']=est[row['road_name']]
			row_dict['est_dev']=est_dev[row['road_name']]

			json_dump.append(row_dict)


print(json.dumps(json_dump))
















